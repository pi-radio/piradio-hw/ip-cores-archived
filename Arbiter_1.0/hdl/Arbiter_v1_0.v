
`timescale 1 ns / 1 ps

	module Arbiter_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S01_AXIS
		parameter integer C_S01_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,

		// Parameters of Axi Master Bus Interface M01_AXIS
		parameter integer C_M01_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M01_AXIS_START_COUNT	= 32
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S01_AXIS
		input wire  s01_axis_aclk,
		input wire  s01_axis_aresetn,
		output wire  s01_axis_tready,
		input wire [C_S01_AXIS_TDATA_WIDTH-1 : 0] s01_axis_tdata,
		input wire [(C_S01_AXIS_TDATA_WIDTH/8)-1 : 0] s01_axis_tstrb,
		input wire  s01_axis_tlast,
		input wire  s01_axis_tvalid,

		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,

		// Ports of Axi Master Bus Interface M01_AXIS
		input wire  m01_axis_aclk,
		input wire  m01_axis_aresetn,
		output wire  m01_axis_tvalid,
		output wire [C_M01_AXIS_TDATA_WIDTH-1 : 0] m01_axis_tdata,
		output wire [(C_M01_AXIS_TDATA_WIDTH/8)-1 : 0] m01_axis_tstrb,
		output wire  m01_axis_tlast,
		input wire  m01_axis_tready
	);
    
    wire [3 : 0] s_axis_tuser1;
    wire [3 : 0] m_axis_tuser1;
    wire [3 : 0] s_axis_tuser2;
    wire [3 : 0] m_axis_tuser2;
    wire m_axis_tready1;
    wire m_axis_tready2;
    wire m_axis_tvalid1;
    wire m_axis_tvalid2;
        
    arbiter_fifo arb_fifo0 (
      .s_aclk(s00_axis_aclk),                // input wire s_aclk
      .s_aresetn(s00_axis_aresetn),          // input wire s_aresetn
      .s_axis_tvalid(s00_axis_tvalid),  // input wire s_axis_tvalid
      .s_axis_tready(s00_axis_tready),  // output wire s_axis_tready
      .s_axis_tdata(s00_axis_tdata),    // input wire [31 : 0] s_axis_tdata
      .s_axis_tlast(s00_axis_tlast),    // input wire s_axis_tlast
      .s_axis_tuser(s_axis_tuser1),    // input wire [3 : 0] s_axis_tuser
      .m_axis_tvalid(m_axis_tvalid1),  // output wire m_axis_tvalid
      .m_axis_tready(m_axis_tready1),  // input wire m_axis_tready
      .m_axis_tdata(m00_axis_tdata),    // output wire [31 : 0] m_axis_tdata
      .m_axis_tlast(m00_axis_tlast),    // output wire m_axis_tlast
      .m_axis_tuser(m_axis_tuser1)    // output wire [3 : 0] m_axis_tuser
);

arbiter_fifo arb_fifo1 (
      .s_aclk(s01_axis_aclk),                // input wire s_aclk
      .s_aresetn(s01_axis_aresetn),          // input wire s_aresetn
      .s_axis_tvalid(s01_axis_tvalid),  // input wire s_axis_tvalid
      .s_axis_tready(s01_axis_tready),  // output wire s_axis_tready
      .s_axis_tdata(s01_axis_tdata),    // input wire [31 : 0] s_axis_tdata
      .s_axis_tlast(s01_axis_tlast),    // input wire s_axis_tlast
      .s_axis_tuser(s_axis_tuser2),    // input wire [3 : 0] s_axis_tuser
      .m_axis_tvalid(m_axis_tvalid2),  // output wire m_axis_tvalid
      .m_axis_tready(m_axis_tready2),  // input wire m_axis_tready
      .m_axis_tdata(m01_axis_tdata),    // output wire [31 : 0] m_axis_tdata
      .m_axis_tlast(m01_axis_tlast),    // output wire m_axis_tlast
      .m_axis_tuser(m_axis_tuser2)    // output wire [3 : 0] m_axis_tuser
);
    assign m_axis_tready1 = m_axis_tvalid1 && m_axis_tvalid2 && m00_axis_tready && m01_axis_tready;
	assign m_axis_tready2 = m_axis_tvalid1 && m_axis_tvalid2 && m00_axis_tready && m01_axis_tready;
    assign m00_axis_tvalid = m_axis_tready1;
    assign m01_axis_tvalid = m_axis_tready2;
    

	
	endmodule
