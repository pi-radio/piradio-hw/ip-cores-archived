
`timescale 1 ns / 1 ps

	module sync_tester_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,
		parameter integer max_symbol = 8,
		parameter integer max_subc = 1023
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
	    input wire  m00_axis_out_aclk,
		input wire  m00_axis_out_aresetn,
		output wire  m00_axis_out_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_out_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_out_tstrb,
		output wire  m00_axis_out_tlast,
		input wire  m00_axis_out_tready
	);


    
	reg [12 : 0] counter = 0;
	reg [5 : 0]  sym_counter = 0;
	assign s00_axis_tready = m00_axis_tready;
	assign m00_axis_tdata = s00_axis_tdata;
	assign m00_axis_tvalid = s00_axis_tvalid;
	
	assign m00_axis_out_tdata[12 : 0] = counter;
	assign m00_axis_out_tdata[18 : 13] = sym_counter;
	
	always @(posedge s00_axis_aclk) begin
	   if(!s00_axis_aresetn) begin
	       counter <= 0;
	       sym_counter <= 0;
	   end
	   else begin
	       if(s00_axis_tvalid && m00_axis_tready)
	           counter <= counter + 1;
	           if(counter == max_subc) begin
	               counter <= 0;
	               if (sym_counter < max_symbol)
	                   sym_counter <= sym_counter + 1;
	               else
	                   sym_counter <= 0;
	           end
	   end
	end

	endmodule
