
`timescale 1 ns / 1 ps

	module csma_ca_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Slave Bus Interface S01_AXI
		parameter integer C_S01_AXI_DATA_WIDTH	= 32,
		parameter integer C_S01_AXI_ADDR_WIDTH	= 4,

		// Parameters of Axi Slave Bus Interface S02_AXI
		parameter integer C_S02_AXI_DATA_WIDTH	= 32,
		parameter integer C_S02_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		// Ports of Axi Slave Bus Interface S01_AXI
		input wire  s01_axi_aclk,
		input wire  s01_axi_aresetn,
		input wire [C_S01_AXI_ADDR_WIDTH-1 : 0] s01_axi_awaddr,
		input wire [2 : 0] s01_axi_awprot,
		input wire  s01_axi_awvalid,
		output wire  s01_axi_awready,
		input wire [C_S01_AXI_DATA_WIDTH-1 : 0] s01_axi_wdata,
		input wire [(C_S01_AXI_DATA_WIDTH/8)-1 : 0] s01_axi_wstrb,
		input wire  s01_axi_wvalid,
		output wire  s01_axi_wready,
		output wire [1 : 0] s01_axi_bresp,
		output wire  s01_axi_bvalid,
		input wire  s01_axi_bready,
		input wire [C_S01_AXI_ADDR_WIDTH-1 : 0] s01_axi_araddr,
		input wire [2 : 0] s01_axi_arprot,
		input wire  s01_axi_arvalid,
		output wire  s01_axi_arready,
		output wire [C_S01_AXI_DATA_WIDTH-1 : 0] s01_axi_rdata,
		output wire [1 : 0] s01_axi_rresp,
		output wire  s01_axi_rvalid,
		input wire  s01_axi_rready,

		// Ports of Axi Slave Bus Interface S02_AXI
		input wire  s02_axi_aclk,
		input wire  s02_axi_aresetn,
		input wire [C_S02_AXI_ADDR_WIDTH-1 : 0] s02_axi_awaddr,
		input wire [2 : 0] s02_axi_awprot,
		input wire  s02_axi_awvalid,
		output wire  s02_axi_awready,
		input wire [C_S02_AXI_DATA_WIDTH-1 : 0] s02_axi_wdata,
		input wire [(C_S02_AXI_DATA_WIDTH/8)-1 : 0] s02_axi_wstrb,
		input wire  s02_axi_wvalid,
		output wire  s02_axi_wready,
		output wire [1 : 0] s02_axi_bresp,
		output wire  s02_axi_bvalid,
		input wire  s02_axi_bready,
		input wire [C_S02_AXI_ADDR_WIDTH-1 : 0] s02_axi_araddr,
		input wire [2 : 0] s02_axi_arprot,
		input wire  s02_axi_arvalid,
		output wire  s02_axi_arready,
		output wire [C_S02_AXI_DATA_WIDTH-1 : 0] s02_axi_rdata,
		output wire [1 : 0] s02_axi_rresp,
		output wire  s02_axi_rvalid,
		input wire  s02_axi_rready,
		
		input wire csma_clk,
		input wire csma_rst,
		output wire [47 : 0] P
	);
wire [31 : 0] delay;
	wire delay_valid;
	wire [47 : 0] status;
	
	wire [31 : 0] P_I, P_Q, P_mag, P_mag_del, dout;
    wire full, empty, wr_rst_busy, rd_rst_busy;
    wire rd_en;
    wire [15 : 0] i_comp, q_comp;
    
    assign s00_axis_tready = 1;

// Instantiation of Axi Bus Interface S01_AXI
	csma_ca_v1_0_S01_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S01_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S01_AXI_ADDR_WIDTH)
	) csma_ca_v1_0_S01_AXI_inst (
		.S_AXI_ACLK(s01_axi_aclk),
		.S_AXI_ARESETN(s01_axi_aresetn),
		.S_AXI_AWADDR(s01_axi_awaddr),
		.S_AXI_AWPROT(s01_axi_awprot),
		.S_AXI_AWVALID(s01_axi_awvalid),
		.S_AXI_AWREADY(s01_axi_awready),
		.S_AXI_WDATA(s01_axi_wdata),
		.S_AXI_WSTRB(s01_axi_wstrb),
		.S_AXI_WVALID(s01_axi_wvalid),
		.S_AXI_WREADY(s01_axi_wready),
		.S_AXI_BRESP(s01_axi_bresp),
		.S_AXI_BVALID(s01_axi_bvalid),
		.S_AXI_BREADY(s01_axi_bready),
		.S_AXI_ARADDR(s01_axi_araddr),
		.S_AXI_ARPROT(s01_axi_arprot),
		.S_AXI_ARVALID(s01_axi_arvalid),
		.S_AXI_ARREADY(s01_axi_arready),
		.S_AXI_RDATA(s01_axi_rdata),
		.S_AXI_RRESP(s01_axi_rresp),
		.S_AXI_RVALID(s01_axi_rvalid),
		.S_AXI_RREADY(s01_axi_rready),
		.delay(delay),
		.delay_valid(delay_valid)
	);

// Instantiation of Axi Bus Interface S02_AXI
	csma_ca_v1_0_S02_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S02_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S02_AXI_ADDR_WIDTH)
	) csma_ca_v1_0_S02_AXI_inst (
		.S_AXI_ACLK(s02_axi_aclk),
		.S_AXI_ARESETN(s02_axi_aresetn),
		.S_AXI_AWADDR(s02_axi_awaddr),
		.S_AXI_AWPROT(s02_axi_awprot),
		.S_AXI_AWVALID(s02_axi_awvalid),
		.S_AXI_AWREADY(s02_axi_awready),
		.S_AXI_WDATA(s02_axi_wdata),
		.S_AXI_WSTRB(s02_axi_wstrb),
		.S_AXI_WVALID(s02_axi_wvalid),
		.S_AXI_WREADY(s02_axi_wready),
		.S_AXI_BRESP(s02_axi_bresp),
		.S_AXI_BVALID(s02_axi_bvalid),
		.S_AXI_BREADY(s02_axi_bready),
		.S_AXI_ARADDR(s02_axi_araddr),
		.S_AXI_ARPROT(s02_axi_arprot),
		.S_AXI_ARVALID(s02_axi_arvalid),
		.S_AXI_ARREADY(s02_axi_arready),
		.S_AXI_RDATA(s02_axi_rdata),
		.S_AXI_RRESP(s02_axi_rresp),
		.S_AXI_RVALID(s02_axi_rvalid),
		.S_AXI_RREADY(s02_axi_rready),
		.status(status[31 : 0])
	);
assign P = status;
assign i_comp = (delay_valid) ? s00_axis_tdata[15 : 0]/*{s00_axis_tdata[31],s00_axis_tdata[24 : 8]} */: 16'h0000;
	assign q_comp = (delay_valid) ? s00_axis_tdata[31 : 16] /*{s00_axis_tdata[63], s00_axis_tdata[54 : 40]}*/ : 16'h0000;
	
	reg [31 : 0] cnt = 0;
	
	always@(posedge csma_clk) begin
	   if(delay_valid && !rd_en)
	       cnt <= cnt + 1;
	end
	
	assign rd_en = (cnt < delay) ? 0 : 1;
	assign P_mag_del = (delay_valid) ? dout : 32'h00000000;
	
csma_fifo fifo (
  .clk(csma_clk),                  // input wire clk
  .srst(0),                // input wire srst
  .din(P_mag),                  // input wire [31 : 0] din
  .wr_en(delay_valid),              // input wire wr_en
  .rd_en(rd_en),              // input wire rd_en
  .dout(dout),                // output wire [31 : 0] dout
  .full(full),                // output wire full
  .empty(empty),              // output wire empty
  .wr_rst_busy(wr_rst_busy),  // output wire wr_rst_busy
  .rd_rst_busy(rd_rst_busy)  // output wire rd_rst_busy
);

dsp_square i_sq (
  .CLK(csma_clk),  // input wire CLK
  .A(i_comp),      // input wire [15 : 0] A
  .B(i_comp),      // input wire [15 : 0] B
  .P(P_I)      // output wire [31 : 0] P
);

dsp_square q_sq (
  .CLK(csma_clk),  // input wire CLK
  .A(q_comp),      // input wire [15 : 0] A
  .B(q_comp),      // input wire [15 : 0] B
  .P(P_Q)      // output wire [31 : 0] P
);

dsp_add add (
  .CLK(csma_clk),        // input wire CLK
  .C(P_I),            // input wire [33 : 0] C
  .CONCAT(P_Q),  // input wire [33 : 0] CONCAT
  .P(P_mag)            // output wire [34 : 0] P
);

dsp_add_sub final_dsp (
  .CLK(csma_clk),  // input wire CLK
  .CONCAT(P_mag),      // input wire [15 : 0] A
  .C(P_mag_del),      // input wire [15 : 0] C
  .P(status)      // output wire [15 : 0] P
);
	endmodule
