// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
// Date        : Tue Nov  9 12:07:21 2021
// Host        : localhost.localdomain running 64-bit openSUSE Tumbleweed
// Command     : write_verilog -force -mode synth_stub
//               /home/george/Documents/piradio_driver_dev/ip_repo/csma_ca_1.0/src/dsp_add_sub/dsp_add_sub_stub.v
// Design      : dsp_add_sub
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "dsp_macro_v1_0_2,Vivado 2021.1" *)
module dsp_add_sub(CLK, C, CONCAT, P)
/* synthesis syn_black_box black_box_pad_pin="CLK,C[31:0],CONCAT[31:0],P[47:0]" */;
  input CLK;
  input [31:0]C;
  input [31:0]CONCAT;
  output [47:0]P;
endmodule
